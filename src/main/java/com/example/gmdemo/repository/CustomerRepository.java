package com.example.gmdemo.repository;

import com.example.gmdemo.model.customer;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerRepository extends MongoRepository<customer, String> {
//    customer findByid(String id);
    customer findBy_id(String _id);
    customer findByEmail(String email);
    customer findByName(String name);
}
