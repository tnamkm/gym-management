package com.example.gmdemo.repository;

import com.example.gmdemo.model.role;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RoleRepository extends MongoRepository<role, String> {

        role findByRole(String role);
    }

