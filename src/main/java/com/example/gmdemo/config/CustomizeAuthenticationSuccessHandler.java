package com.example.gmdemo.config;

import com.example.gmdemo.model.customer;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.example.gmdemo.repository.CustomerRepository;
import com.example.gmdemo.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class CustomizeAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Autowired
    CustomUserDetailsService customUserDetailsService;
    @Autowired
    CustomerRepository userRepository;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
        //set our response to OK status
        response.setStatus(HttpServletResponse.SC_OK);

        String username = authentication.getName();

        customer user = customUserDetailsService.findUserByEmail(username);
        userRepository.save(user);

        for (GrantedAuthority auth : authentication.getAuthorities()) {
            if ("USER".equals(auth.getAuthority())) {
                response.sendRedirect("/mainpage_PT");
            }
            if ("ADMIN".equals(auth.getAuthority())) {
                response.sendRedirect("/admin");
            }
        }
    }
}