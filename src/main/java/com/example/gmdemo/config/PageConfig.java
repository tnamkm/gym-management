package com.example.gmdemo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.context.annotation.Bean;


@Configuration
public class PageConfig implements WebMvcConfigurer {
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/homepage").setViewName("homepage");
        registry.addViewController("/").setViewName("homepage");
        registry.addViewController("/dashboard").setViewName("dashboard");
        registry.addViewController("/mainpage_PT").setViewName("mainpage_PT");
        registry.addViewController("/mainpage_chart").setViewName("mainpage_chart");
        registry.addViewController("/signup").setViewName("signup");

        registry.addViewController("/login").setViewName("login");
    }
}