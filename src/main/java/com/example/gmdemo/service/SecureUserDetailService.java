package com.example.gmdemo.service;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

@Service
public class SecureUserDetailService {
    public User getCurrentUser() throws ClassCastException {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
