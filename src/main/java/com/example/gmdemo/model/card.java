package com.example.gmdemo.model;

public class card {
    private String id;
    private String userID;
    private String userName;
    private String type;
    private String activeDay;
    private String expireDay;

    public card(){

    }

    public card(String id, String userID, String userName, String type, String activeDay, String expireDay ){
        this.id = id;
        this.userID = userID;
        this.userName = userName;
        this.type = type;
        this. activeDay = activeDay;
        this.expireDay = expireDay;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getActiveDay() {
        return activeDay;
    }

    public void setActiveDay(String activeDay) {
        this.activeDay = activeDay;
    }

    public String getExpireDay() {
        return expireDay;
    }

    public void setExpireDay(String expireDay) {
        this.expireDay = expireDay;
    }
}
