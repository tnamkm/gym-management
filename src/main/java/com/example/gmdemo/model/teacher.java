package com.example.gmdemo.model;

public class teacher {
    private String id;
    private String name;
    private String classID;

    public teacher(){

    }

    public teacher(String id, String name, String classID){
        this.id = id;
        this.name = name;
        this.classID = classID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }
}
