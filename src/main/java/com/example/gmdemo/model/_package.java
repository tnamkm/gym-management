package com.example.gmdemo.model;

public class _package {
    private String id;
    private String duration;
    private String description;
    private String price;

    public _package(){

    }

    public _package(String id, String duration, String description,String price){
        this.id = id;
        this.duration = duration;
        this.description = description;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
