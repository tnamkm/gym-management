package com.example.gmdemo;

import com.example.gmdemo.model.role;
import com.example.gmdemo.repository.RoleRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class GmDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(GmDemoApplication.class, args);
    }

    @Bean
    CommandLineRunner init(RoleRepository roleRepository) {

        return args -> {

            role adminRole = roleRepository.findByRole("ADMIN");
            if (adminRole == null) {
                role newAdminRole = new role();
                newAdminRole.setRole("ADMIN");
                roleRepository.save(newAdminRole);
            }

            role userRole = roleRepository.findByRole("USER");
            if (userRole == null) {
                role newUserRole = new role();
                newUserRole.setRole("USER");
                roleRepository.save(newUserRole);
            }
        };

    }
}
