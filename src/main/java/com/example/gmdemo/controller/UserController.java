package com.example.gmdemo.controller;

import com.example.gmdemo.model.customer;
import com.example.gmdemo.repository.CustomerRepository;
import com.example.gmdemo.service.CustomUserDetailsService;
import javax.validation.Valid;
import com.google.gson.JsonObject;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import com.example.gmdemo.service.SecureUserDetailService;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.FileInputStream;
import java.io.IOException;
import org.apache.commons.io.FilenameUtils;

import java.io.InputStream;

@Controller
public class UserController {
    @Autowired
    private CustomUserDetailsService userService;
    private CustomerRepository userRepository;

    private SecureUserDetailService secureUserDetailService;

    public UserController(CustomerRepository userRepository) {
        this.userRepository = userRepository;
    }


    @RequestMapping(value = "/profile",method = RequestMethod.GET)
    public ModelAndView editProfile() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        customer user = userService.findUserByEmail(auth.getName());
        modelAndView.addObject("currentUser", user);
        modelAndView.setViewName("profile");
        return modelAndView;
    }

    @RequestMapping(value = "/user/getCurrentUser", method = RequestMethod.GET)
    @ResponseBody
    public String getCurrentUser() {
        JsonObject jsonObject = new JsonObject();
        SecureUserDetailService secureUserDetailService = new SecureUserDetailService();
        String currentUserEmail = secureUserDetailService.getCurrentUser().getUsername();
        customer currentUser = userRepository.findByEmail(currentUserEmail);
        jsonObject.addProperty("id", currentUser.getId());
        jsonObject.addProperty("name", currentUser.getName());
        jsonObject.addProperty("email", currentUser.getEmail());
        return jsonObject.toString();
    }

    @RequestMapping(value = "/user/getCurrentUserId", method = RequestMethod.GET)
    @ResponseBody
    public String getCurrentUserId() {
        SecureUserDetailService secureUserDetailService = new SecureUserDetailService();
        String currentUserEmail = secureUserDetailService.getCurrentUser().getUsername();
        customer currentUser = userRepository.findByEmail(currentUserEmail);
        return currentUser.getId();
    }

    @RequestMapping(value = "/edit/fullName", method = RequestMethod.POST)
    @ResponseBody
    public void editFullname(String id, String fullName) {
        customer user = userRepository.findBy_id(id);
        user.setName(fullName);
        userRepository.save(user);
    }

    @RequestMapping(value = "/edit/birthDay", method = RequestMethod.POST)
    @ResponseBody
    public void editBirthday(String id, String birthDay) {
        customer user = userRepository.findBy_id(id);
        user.setBirthday(birthDay);
        userRepository.save(user);
    }

    @RequestMapping(value = "/edit/gender", method = RequestMethod.POST)
    @ResponseBody
    public void editGender(String id, String gender) {
        customer user = userRepository.findBy_id(id);
        user.setGender(gender);
        userRepository.save(user);
    }

    @RequestMapping(value = "/edit/phoneNumber", method = RequestMethod.POST)
    @ResponseBody
    public void editPhoneNumber(String id, String phoneNumber) {
        customer user = userRepository.findBy_id(id);
        user.setPhoneNumber(phoneNumber);
        userRepository.save(user);
    }

    @RequestMapping(value = "/edit/address", method = RequestMethod.POST)
    @ResponseBody
    public void editAddress(String id, String address) {
        customer user = userRepository.findBy_id(id);
        user.setAddress(address);
        userRepository.save(user);
    }

    @RequestMapping(value = "/edit/job", method = RequestMethod.POST)
    @ResponseBody
    public void editJob(String id, String job) {
        customer user = userRepository.findBy_id(id);
        user.setJob(job);
        userRepository.save(user);
    }

    @RequestMapping(value = "/edit/income", method = RequestMethod.POST)
    @ResponseBody
    public void editIncome(String id, String income) {
        customer user = userRepository.findBy_id(id);
        user.setIncome(income);
        userRepository.save(user);
    }

    @RequestMapping(value = "/edit/trainingHistory", method = RequestMethod.POST)
    @ResponseBody
    public void editTrainingHistory(String id, String history) {
        customer user = userRepository.findBy_id(id);
        user.setTrainingHistory(history);
        userRepository.save(user);
    }

    @RequestMapping(value = "/user/saveAvatar", method = RequestMethod.POST)
    @ResponseBody
    public String saveAvatarToDatabase(MultipartFile file, String userId) throws IOException {
        String fileName = userId + '.' + FilenameUtils.getExtension(file.getOriginalFilename());
        customer user = userRepository.findBy_id(userId);
        //Save image metadata to MongoDB
        user.setAvatar(fileName);
        userRepository.save(user);
        return "User avatar saved";
    }

//    @RequestMapping(value = "/user/getAvatar/{userId}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
//    @ResponseBody
//    public byte[] getUserAvatar(@PathVariable String userId) throws IOException {
//        customer user = userRepository.findBy_id(userId);
//        try {
//            String blobString = "user_images/" + user.getAvatar();
//            Blob blob = storageClient.bucket().get(blobString);
//            return blob.getContent();
//        } catch (NullPointerException ignored) {
//            FileInputStream defaultAvatar = new FileInputStream("target/classes/static/images/avatar-default.jpg");
//            return IOUtils.toByteArray(defaultAvatar);
//        }
//    }

//    @RequestMapping(value = "/setting",method = RequestMethod.GET)
//    public ModelAndView setting() {
//        ModelAndView modelAndView = new ModelAndView();
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        customer user = userService.findUserByEmail(auth.getName());
//        modelAndView.addObject("currentUser", user);
//        modelAndView.setViewName("setting");
//        return modelAndView;
//    }

    @RequestMapping(value = "/setting", method = RequestMethod.POST)
    public ModelAndView changePassword(ModelAndView modelAndView, String oldPassword, String newPassword) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        String currentUserEmail = secureUserDetailService.getCurrentUser().getUsername();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            customer user = userService.findUserByEmail(currentUserName);
            if (user != null && userService.matchPassword(user, oldPassword)) {
                userService.changePassword(user, newPassword);
                modelAndView.addObject("success_change", "Change password successfully!");
                modelAndView.addObject("currentUser");
                modelAndView.setViewName("login");
            } else {
//                modelAndView.addObject("currentUser", userRepository.findByEmail(currentUserEmail));
                modelAndView.addObject("errorMessage_token", "This email does not exist!");
//                modelAndView.addObject("currentUser");
                modelAndView.setViewName("login");
            }
        }
        return modelAndView;
    }

    @RequestMapping(value = "/setting", method = RequestMethod.GET)
    public ModelAndView showChangePasswordPage() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            ModelAndView modelAndView = new ModelAndView();
            SecureUserDetailService secureUserDetailService = new SecureUserDetailService();
            String currentUserEmail = secureUserDetailService.getCurrentUser().getUsername();
            modelAndView.addObject("currentUser", userService.findUserByEmail(currentUserEmail));
            modelAndView.setViewName("setting");
            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.addObject("warning", "you are an anonymous!");
            modelAndView.setViewName("login");
            return modelAndView;
        }
    }
}