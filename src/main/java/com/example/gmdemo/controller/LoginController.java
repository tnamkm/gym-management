package com.example.gmdemo.controller;

import com.example.gmdemo.model.customer;
import com.example.gmdemo.service.CustomUserDetailsService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.net.UnknownHostException;

@Controller
public class LoginController {
    @Autowired
    private CustomUserDetailsService userService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }

    @RequestMapping(value = "/signup-success", method = RequestMethod.GET)
    public ModelAndView signupsucccess() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("signup-success");
        return modelAndView;
    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public ModelAndView signup() {
        ModelAndView modelAndView = new ModelAndView();
        customer user = new customer();
        modelAndView.addObject("user", user);
        modelAndView.setViewName("signup");
        return modelAndView;
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ModelAndView createNewUser(String email, String password, String name)throws UnknownHostException {
        ModelAndView modelAndView = new ModelAndView();
        customer newUser = new customer(email, password, name);
        customer userExists = userService.findUserByEmail(newUser.getEmail());

//        if (userExists != null) {
//            bindingResult
//                    .rejectValue("email", "error.user",
//                            "There is already a user registered with the username provided");
//        }
//        if (bindingResult.hasErrors()) {
//            modelAndView.setViewName("signup");
//        } else {
//            userService.saveUser(user);
//            modelAndView.addObject("successMessage", "User has been registered successfully");
//            modelAndView.addObject("user", new customer());
//            modelAndView.setViewName("login");
//
//        }
//        return modelAndView;

        if (userExists != null) {
            modelAndView.addObject("err_exist", "There is already a user registered with the username provided");
            modelAndView.setViewName("signup");
        } else {
            userService.saveUser(newUser);
//            sendEmailService.sendVerificationEmail(newUser.getEmail());
            modelAndView.addObject("signup_confirm", "Open email to confirm account!");
            modelAndView.addObject("user", new customer());
            modelAndView.setViewName("signup-success");
        }
        return modelAndView;
    }
    @RequestMapping(value = "/mainpage_PT", method = RequestMethod.GET)
    public ModelAndView dashboard() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        customer user = userService.findUserByEmail(auth.getName());
        modelAndView.addObject("currentUser", user);
//        modelAndView.addObject("fullName", "Welcome " + user.getName());
//        modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");
        modelAndView.setViewName("mainpage_PT");
        return modelAndView;
    }


    @RequestMapping(value = "/mainpage_chart",method = RequestMethod.GET)
    public ModelAndView pagechart(){
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        customer user = userService.findUserByEmail(auth.getName());
        modelAndView.addObject("currentUser", user);
        modelAndView.setViewName("mainpage_chart");
        return modelAndView;
    }

    @RequestMapping(value = "/mainpage_package",method = RequestMethod.GET)
    public ModelAndView pagepackage(){
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        customer user = userService.findUserByEmail(auth.getName());
        modelAndView.addObject("currentUser", user);
        modelAndView.setViewName("mainpage_package");
        return modelAndView;
    }

    @RequestMapping(value = "/mainpage_response",method = RequestMethod.GET)
    public ModelAndView pageresponse(){
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        customer user = userService.findUserByEmail(auth.getName());
        modelAndView.addObject("currentUser", user);
        modelAndView.setViewName("mainpage_response");
        return modelAndView;
    }

    @RequestMapping(value = "/mainpage_schedule",method = RequestMethod.GET)
    public ModelAndView pageschedule(){
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        customer user = userService.findUserByEmail(auth.getName());
        modelAndView.addObject("currentUser", user);
        modelAndView.setViewName("mainpage_schedule");
        return modelAndView;
    }


    @RequestMapping(value = {"/homepage"}, method = RequestMethod.GET)
    public ModelAndView home() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("homepage");
        return modelAndView;
    }
}
